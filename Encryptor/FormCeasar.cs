﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;


namespace Encryptor
{
    public partial class FormCeasar : Form
    {
       
        private char[] alphabetLowerCase = new char[33] {'а','б','в','г','д','е','ё','ж',
                'з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч',
                'ш','щ','ъ','ы','ь','э','ю','я'};
        private char[] alphabetUpperCase = new char[33];
        private char[] alphabetNum = new char[10] { '0', '1', '2', '3', '4', '5', '6',
            '7', '8', '9'};
        private char[][] alphabets = new char[3][];


        public FormCeasar()
        {
            InitializeComponent();
            alphabetUpperCase = MakeAlphabetUpperCase(alphabetLowerCase);
            alphabets = MakeArrayOfAlphabets(alphabets);                
        }
        //
        // EVENTS
        //
        // Open file and get text from it
        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|Word document (*.doc?) |*.doc?";
            openFileDialog1.FileName = "";
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string pathToFile = openFileDialog1.FileName;
                string extension = pathToFile.Substring(pathToFile.LastIndexOf("."));
                if (extension.Equals(".docx") || extension.Equals(".doc"))
                    OpenDocxFile(pathToFile);
                else if (extension.Equals(".txt"))
                    OpenTxtFile(pathToFile);
                else MessageBox.Show("Неверный формат входных данных");
            }

        }
        // Encrypt text using Ceasar cipher
        private void btnEncrypt_Click(object sender, EventArgs e)
        {
            int rot;
            string sh = tbShift.Text;
            if (sh != "")
            {
                try
                {
                    rot = int.Parse(sh);

                    if (!rbToLeft.Checked && !rbToRight.Checked)
                    {
                        MessageBox.Show("Выберите направлнения сдвига!");
                    }
                    else if (richTextBoxText.Text.Equals(""))
                    {
                        MessageBox.Show("Откройте файл с тексом для зашифровки!");
                    }
                    else
                    {
                        richTextBoxChanged.Text = EncryptText(rot,
                            richTextBoxText.Text, rbToRight.Checked);
                    }

                }
                catch (Exception x)
                {
                    MessageBox.Show("Значение поля \"Cдвиг\" должно быть целочисленным!");
                }
            }
            else
            {
                MessageBox.Show("Поле \"Cдвиг\" не может быть пустым!");
            }

        }
        // Decrypt text using Ceasar cipher
        private void btnDecrypt_Click(object sender, EventArgs e)
        {
            int rot;
            string sh = tbShift.Text;
            if (sh != "")
            {
                try
                {
                    rot = int.Parse(sh);

                    if (!rbToLeft.Checked && !rbToRight.Checked)
                    {
                        MessageBox.Show("Выберите направлнения сдвига!");
                    }
                    else if (richTextBoxText.Text.Equals(""))
                    {
                        MessageBox.Show("Откройте файл с тексом для дешифровки!");
                    }
                    else
                    {
                        richTextBoxChanged.Text = DecryptText(rot, 
                            richTextBoxText.Text, rbToRight.Checked);
                    }

                }
                catch (Exception x)
                {
                    MessageBox.Show("Значение поля \"Cдвиг\" должно быть целочисленным!");
                }
            }
            else
            {
                MessageBox.Show("Поле \"Cдвиг\" не может быть пустым!");
            }
        }
        // Save decrypted/encrypted file
        private void btnSave_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|Word document (*.docx) |*.docx";
            saveFileDialog1.FileName = "FileName";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string pathToFile = saveFileDialog1.FileName;
                string extension = pathToFile.Substring(pathToFile.LastIndexOf("."));
                if (extension.Equals(".docx") || extension.Equals(".doc"))
                    SaveDocxFile(pathToFile);
                else if (extension.Equals(".txt"))
                {
                    SaveTxtFile(pathToFile);

                }
                else MessageBox.Show("Неверный формат входных данных");
            }

        }
        //
        // MAKE ALPHABETS
        //
        //
        private char[] MakeAlphabetUpperCase(char[] alphabetLowerCase)
        {
            for (int i = 0; i < alphabetLowerCase.Length; i++)
            {
                alphabetUpperCase[i] = Convert.ToChar(alphabetLowerCase[i].ToString().ToUpper());
            }
            return alphabetUpperCase;
        }
        //
        private char[][] MakeArrayOfAlphabets(char[][] alphabets)
        {
            alphabets[0] = alphabetLowerCase;
            alphabets[1] = alphabetUpperCase;
            alphabets[2] = alphabetNum;
            return alphabets;
        }
        //
        // FOR OPEN_FILE BUTTON
        //
        // Open the WordApp and WordDocument and extract text from it
        public void OpenDocxFile(string pathToDocx)
        {
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                richTextBoxText.Clear();
                Word.Application wordApp = new Word.Application();
                wordApp.ShowAnimation = false;
                wordApp.Visible = false;
                Word.Document docFile = wordApp.Documents.Open(pathToDocx);
                wordApp.Activate();

                richTextBoxText.Text = GetTextFromDocxFile(docFile);
                docFile.Close();
                wordApp.Quit();
                Cursor.Current = Cursors.Default;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);

            }
        }
        // Parse the docx file
        public string GetTextFromDocxFile(Word.Document docFile)
        {
            string textFromDocx = "";
            for (int i = 1; i < docFile.Paragraphs.Count + 1; i++)
            {
                string currString = docFile.Paragraphs[i].Range.Text.ToString();
                textFromDocx += currString;
            }

            return textFromDocx;
        }
        // Open the txt file and extract text from it
        public void OpenTxtFile(string pathToTxt)
        {
            try
            {
                richTextBoxText.Clear();
                richTextBoxText.Text =  File.ReadAllText(pathToTxt, 
                    Encoding.GetEncoding(1251));
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        //
        // FOR ENCRYPT_BUTTON
        //
        // Encrypt text char at a time
        public string EncryptText(int rot, string text, bool ToRight)
        {
            string textChanged = "";
            richTextBoxChanged.Clear();

            foreach (char c in text)
            {
                bool flag = IsLetterChanged(c, rot, ToRight,ref textChanged);
                if (!flag)
                {
                    textChanged += c;
                }
            }
            return textChanged;
        }
        //
        // FOR DECRYPT_BUTTON
        //
        // Dencrypt text char at a time
        public string DecryptText(int rot, string text, bool ToRight)
        {
            string textChanged;
            text = richTextBoxText.Text;
            textChanged = "";
            richTextBoxChanged.Clear();

            foreach (char c in text)
            {
                bool flag = IsLetterChanged(c, rot, !ToRight, ref textChanged);


                if (!flag)
                {
                    textChanged += c;
                }
            }
            return textChanged;
        }
        // Return true if Letter was changed
        public bool IsLetterChanged(char c, int rot, bool ToRight, ref string textChanged)
        {
            bool flag = false;
            foreach (char[] alphabet in alphabets)
            {
                rot = GetProperRot(rot, alphabet);

                for (int i = 0; i < alphabet.Length; i++)
                {
                    if (c.Equals(alphabet[i]))
                    {
                        if (ToRight)
                        {
                            textChanged += ChangeToRight(i, rot, alphabet);
                            flag = true;
                        }
                        else
                        {
                            textChanged += ChangeToLeft(i, rot, alphabet);
                            flag = true;
                        }

                    }
                }
            }
            return flag;
        }
        // Get proper rotation value
        public int GetProperRot(int rot, char[] alphabet)
        {
            if (rot > alphabet.Length)
            {
                rot = rot % alphabet.Length;
            }
            return rot;
        }
        // Encrypt/decrypt with rotation to right
        public char ChangeToRight(int i, int rot, char[] alphabet)
        {
            char cChanged;

            if (i + rot > (alphabet.Length - 1))
            {
                cChanged = alphabet[i + rot - alphabet.Length];

            }
            else
            {
                cChanged = alphabet[i + rot];

            }

            return cChanged;
        }
        // Encrypt/ decrypt with rotation to left
        public char ChangeToLeft(int i, int rot, char[] alphabet)
        {
            char cChanged;
            if (i - rot >= 0)
            {
                cChanged = alphabet[i - rot];

            }
            else
            {
                cChanged = alphabet[i - rot + alphabet.Length];

            }
            return cChanged;
        }
        //
        // FOR SAFE_BUTTON
        //
        // Open the WordApp, create WordDocument and write text to docx
        public void SaveDocxFile(string pathToDocx)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                Word.Application wordApp = new Word.Application();
                wordApp.ShowAnimation = false;
                wordApp.Visible = false;
                Word.Document document = wordApp.Documents.Add();
                document.Content.SetRange(0, 0);
                document.Content.Text = richTextBoxChanged.Text;
                document.SaveAs2(pathToDocx);
                document.Close();
                document = null;
                wordApp.Quit();
                MessageBox.Show("Вы успешно сохранили файл!");
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString());
            }
            Cursor.Current = Cursors.Default;

        }
        // Open the new streamwriter and write to file
        public void SaveTxtFile(string pathToTxt)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(pathToTxt, false, Encoding.GetEncoding(1251)))
                {
                    sw.Write(richTextBoxChanged.Text, Encoding.GetEncoding(1251));
                    sw.Close();
                    MessageBox.Show("Вы успешно сохранили файл!");
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

        }
    }
}
