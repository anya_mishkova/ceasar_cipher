﻿namespace Encryptor
{
    partial class FormCeasar
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOpenFile = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnDecrypt = new System.Windows.Forms.Button();
            this.btnEncrypt = new System.Windows.Forms.Button();
            this.rbToRight = new System.Windows.Forms.RadioButton();
            this.rbToLeft = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.tbShift = new System.Windows.Forms.TextBox();
            this.richTextBoxText = new System.Windows.Forms.RichTextBox();
            this.richTextBoxChanged = new System.Windows.Forms.RichTextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnOpenFile
            // 
            this.btnOpenFile.Location = new System.Drawing.Point(454, 407);
            this.btnOpenFile.Name = "btnOpenFile";
            this.btnOpenFile.Size = new System.Drawing.Size(120, 40);
            this.btnOpenFile.TabIndex = 0;
            this.btnOpenFile.Text = "Открыть";
            this.btnOpenFile.UseVisualStyleBackColor = true;
            this.btnOpenFile.Click += new System.EventHandler(this.btnOpenFile_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnDecrypt
            // 
            this.btnDecrypt.Location = new System.Drawing.Point(454, 180);
            this.btnDecrypt.Name = "btnDecrypt";
            this.btnDecrypt.Size = new System.Drawing.Size(120, 40);
            this.btnDecrypt.TabIndex = 4;
            this.btnDecrypt.Text = "Дешифровать";
            this.btnDecrypt.UseVisualStyleBackColor = true;
            this.btnDecrypt.Click += new System.EventHandler(this.btnDecrypt_Click);
            // 
            // btnEncrypt
            // 
            this.btnEncrypt.Location = new System.Drawing.Point(316, 180);
            this.btnEncrypt.Name = "btnEncrypt";
            this.btnEncrypt.Size = new System.Drawing.Size(120, 40);
            this.btnEncrypt.TabIndex = 5;
            this.btnEncrypt.Text = "Зашифровать";
            this.btnEncrypt.UseVisualStyleBackColor = true;
            this.btnEncrypt.Click += new System.EventHandler(this.btnEncrypt_Click);
            // 
            // rbToRight
            // 
            this.rbToRight.AutoSize = true;
            this.rbToRight.Location = new System.Drawing.Point(16, 190);
            this.rbToRight.Name = "rbToRight";
            this.rbToRight.Size = new System.Drawing.Size(75, 21);
            this.rbToRight.TabIndex = 6;
            this.rbToRight.Text = "вправо";
            this.rbToRight.UseVisualStyleBackColor = true;
            // 
            // rbToLeft
            // 
            this.rbToLeft.AutoSize = true;
            this.rbToLeft.Location = new System.Drawing.Point(97, 190);
            this.rbToLeft.Name = "rbToLeft";
            this.rbToLeft.Size = new System.Drawing.Size(67, 21);
            this.rbToLeft.TabIndex = 7;
            this.rbToLeft.TabStop = true;
            this.rbToLeft.Text = "влево";
            this.rbToLeft.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(170, 192);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "на";
            // 
            // tbShift
            // 
            this.tbShift.Location = new System.Drawing.Point(201, 190);
            this.tbShift.Name = "tbShift";
            this.tbShift.Size = new System.Drawing.Size(48, 22);
            this.tbShift.TabIndex = 9;
            this.tbShift.Text = "1";
            // 
            // richTextBoxText
            // 
            this.richTextBoxText.Location = new System.Drawing.Point(16, 46);
            this.richTextBoxText.Name = "richTextBoxText";
            this.richTextBoxText.Size = new System.Drawing.Size(558, 116);
            this.richTextBoxText.TabIndex = 10;
            this.richTextBoxText.Text = "";
            // 
            // richTextBoxChanged
            // 
            this.richTextBoxChanged.Location = new System.Drawing.Point(16, 262);
            this.richTextBoxChanged.Name = "richTextBoxChanged";
            this.richTextBoxChanged.Size = new System.Drawing.Size(558, 116);
            this.richTextBoxChanged.TabIndex = 11;
            this.richTextBoxChanged.Text = "";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(316, 407);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(120, 40);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 17);
            this.label2.TabIndex = 13;
            this.label2.Text = "Исходный текст:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 229);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 17);
            this.label3.TabIndex = 14;
            this.label3.Text = "Результат: ";
            // 
            // FormCeasar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(588, 463);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.richTextBoxChanged);
            this.Controls.Add(this.richTextBoxText);
            this.Controls.Add(this.tbShift);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rbToLeft);
            this.Controls.Add(this.rbToRight);
            this.Controls.Add(this.btnEncrypt);
            this.Controls.Add(this.btnDecrypt);
            this.Controls.Add(this.btnOpenFile);
            this.Name = "FormCeasar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Шифр Цезаря";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOpenFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnDecrypt;
        private System.Windows.Forms.Button btnEncrypt;
        private System.Windows.Forms.RadioButton rbToRight;
        private System.Windows.Forms.RadioButton rbToLeft;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbShift;
        private System.Windows.Forms.RichTextBox richTextBoxText;
        private System.Windows.Forms.RichTextBox richTextBoxChanged;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

