﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Encryptor;
using Xunit;
using Word = Microsoft.Office.Interop.Word;

namespace Encryptor.Test
{
    public class EncryptorTests
    {


        [Theory]
        [InlineData(64, 12, 4)] // сдвиг больше кол-ва символов в алфавите
        [InlineData(5, 12, 5)] // сдвиг меньше кол-ва символов в алфавите
        public void GetProperRot_Test(int rot, int n, int expected)
        {
            char[] alphabet = new char[n];

            int actual = new FormCeasar().GetProperRot(rot, alphabet);
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData('Щ', true)] // элемент алфавита
        [InlineData('2', true)] // элемент алфавита
        [InlineData('!', false)] // не элемент алфавита
        [InlineData(' ', false)] // не элемент алфавита
        public void IsLetterChanged_Test(char c, bool expected)
        {
            string str = "";
            bool actual = new FormCeasar().IsLetterChanged(c, 1, true, ref str);

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(2, 3, new char[33] {'а','б','в','г','д','е','ё','ж',
                'з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч',
                'ш','щ','ъ','ы','ь','э','ю','я'}, 'е') ] // индекс в границах массива, 
        // сдвиг меньше размера алфавита, правильная работа шифра
        [InlineData(30,5, new char[33] {'а','б','в','г','д','е','ё','ж',
                'з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч',
                'ш','щ','ъ','ы','ь','э','ю','я'}, 'в')] // при сдвиге индекс не должен
        // выйти за рамки массива
        public void ChangeToRight_Test(int i, int rot, char[] alphabet, char expected)
        {
            char actual = new FormCeasar().ChangeToRight(i, rot, alphabet);

            Assert.Equal(expected, actual);
        }


        [Theory]
        [InlineData(2, 3, new char[33] {'а','б','в','г','д','е','ё','ж',
                'з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч',
                'ш','щ','ъ','ы','ь','э','ю','я'}, 'я')] // индекс в границах массива, 
        // сдвиг меньше размера алфавита, правильная работа шифра
        [InlineData(1, 5, new char[33] {'а','б','в','г','д','е','ё','ж',
                'з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч',
                'ш','щ','ъ','ы','ь','э','ю','я'}, 'ь')] // при сдвиге индекс не должен
        // выйти за рамки массива
        public void ChangeToLeft_Test(int i, int rot, char[] alphabet, char expected)
        {
            char actual = new FormCeasar().ChangeToLeft(i, rot, alphabet);

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(5, "аБ!вяN", true,"еЁ!ждN")] // проверка на изменения символов
        // только из алфавитов
        [InlineData(98, "мама", true, "ляля")] // сдвиг больше размера алфавита
        [InlineData(1,"", false, "")] // пустая строка
        void EncryptText_Test(int rot, string text, bool ToRight, string expected)
        {
            string actual = new FormCeasar().EncryptText(rot,text, ToRight);

            Assert.Equal(expected, actual);
        }

    }
}
